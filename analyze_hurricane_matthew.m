% This file uses MATLAB to draw a picture of the surface wind speed at the beginning of the tutorial simulation of Hurricane Matthew.

% Make sure that your directory also contains the following files:
% "wrfout_d01_2016-10-06_00:00:00", the output .nc file from the tutorial
% "brewermap.m", a helper function that makes pretty colormaps (also available in this repository)

XLAT = double(ncread('wrfout_d01_2016-10-06_00:00:00','XLAT'));
XLON = double(ncread('wrfout_d01_2016-10-06_00:00:00','XLONG'));
time = double(ncread('wrfout_d01_2016-10-06_00:00:00','XTIME')); time = time/60;
mask = double(ncread('wrfout_d01_2016-10-06_00:00:00','LANDMASK')); mask = mask(:,:,1)';
U = double(ncread('wrfout_d01_2016-10-06_00:00:00','U10'));
V = double(ncread('wrfout_d01_2016-10-06_00:00:00','V10'));
WIND = sqrt(U.^2 + V.^2);
TREFHT = double(ncread('wrfout_d01_2016-10-06_00:00:00','T2')) - 273.15;
PRECT = double(ncread('wrfout_d01_2016-10-06_00:00:00','RAINC')) + double(ncread('wrfout_d01_2016-10-06_00:00:00','RAINNC'));

figure;
l_colb = 9; % number of colors in colormap
fc = brewermap(l_colb,'YlOrRd'); % choose desired color scheme
mmin = 0; mmax = 30; % colorbar limits
v = mmin:(mmax-mmin)/l_colb:mmax; % colorbar spacing
colormap(fc) % sets the colormap to the colors chosen above
worldmap([16 39],[-87 -63]) % create a map using these lat/lon boundaries
box on; hold on;
contourfm(XLAT(:,:,1),XLON(:,:,1),WIND(:,:,1),v,'LineStyle','none') % draw contour on map
load coastlines; hold on;
plotm(coastlat,coastlon,'k') % draw the coastlines in black
caxis([mmin mmax]) % set colorbar limits
colorbar % create colorbar
title('Surface wind speed (m/s), time +0h'); % create title